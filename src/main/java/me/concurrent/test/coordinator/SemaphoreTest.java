package me.concurrent.test.coordinator;

import java.util.concurrent.Semaphore;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、原理：<br>
 * 使用Semaphore先构造资源数量，线程调用acquire申请一个资源，申请到后执行相应操作然后归还资源，否则申请失败进入等待队列等待。<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月8日下午10:38:57
 */
public class SemaphoreTest extends TestCase {

  @Test
  public void test1() throws InterruptedException {
    Semaphore semaphore = new Semaphore(2);
    for (int i = 0; i < 5; i++) {
      Thread u = new Thread(new User(semaphore));
      u.start();
    }
    Thread.sleep(3000);
  }

  class User implements Runnable {
    Semaphore semaphore;

    User(Semaphore semaphore) {
      this.semaphore = semaphore;
    }

    @Override
    public void run() {
      try {
        System.out.println(Thread.currentThread().getName() + "用户正在申请打印机");
        semaphore.acquire();
        System.out.println(Thread.currentThread().getName() + "申请到，正在打印");
        semaphore.release();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
