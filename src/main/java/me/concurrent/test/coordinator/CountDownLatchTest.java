package me.concurrent.test.coordinator;

import java.util.Random;
import java.util.concurrent.CountDownLatch;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、CountDownLatch原理。 <br>
 * 可以让一个（组）线程一直等待直到另一个（组）线程执行完毕再执行。<br>
 * CountDownLatch.Sync继承了AQS，复写了AQS共享锁获取tryAcquireShared和释放tryReleaseShared两个方法，<br>
 * 在构造时初始化AQS维持的计数器，当需要就位的线程都就位时，处于等待的线程将被唤醒。<br>
 * 2、CountDownLatch如何利用AQS协调线程工作。<br>
 * 需要等待的线程调用await后加入等待队列，需要就位的线程调用countDown后检测计数器状态，为0时逐个唤醒整个等待的队列。<br>
 * 3、做好异常处理。worker线程一定要做好异常处理，如果途中出现了异常，boss线程将会无限等待，死锁。<br>
 * <br>
 * 
 * 扩展：<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月6日下午3:48:40
 */
public class CountDownLatchTest extends TestCase {

  @Test
  public void test1() throws InterruptedException {
    System.out.println();
    int workerNum = 5;// 工人线程数量
    CountDownLatch latch = new CountDownLatch(workerNum);

    Thread boss = new Thread(new Boss(latch));
    boss.start();
    Thread manager = new Thread(new Manager(latch));
    manager.start();

    for (int i = 0; i < workerNum; i++) {
      Thread worker = new Thread(new Worker(latch, i));
      worker.start();
    }
    Thread.sleep(1000);
  }

  @Test
  public void testDeadLock() throws InterruptedException {
    System.out.println();
    int workerNum = 5;// 工人线程数量
    CountDownLatch latch = new CountDownLatch(workerNum);

    Thread boss = new Thread(new Boss(latch));
    boss.start();

    for (int i = 0; i < workerNum; i++) {
      Thread worker = new Thread(new UnqualifiedWorker(latch, i));
      worker.start();
    }
    Thread.sleep(1000);
  }

  /**
   * 工人
   */
  class Worker implements Runnable {

    CountDownLatch latch;
    private int    workerId;

    Worker(CountDownLatch latch, int workerId) {
      this.latch = latch;
      this.workerId = workerId;
    }

    @Override
    public void run() {
      System.out.println(Thread.currentThread().getName() + "," + workerId + "号工人已经就绪");
      int waitTime = new Random().nextInt(9);
      try {
        Thread.sleep(waitTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      try {
        if (waitTime % 2 == 0 && waitTime >= 5) {
          System.out.println(Thread.currentThread().getName() + "," + workerId + "号工人发生了意外...boss线程无法执行");
          throw new RuntimeException(Thread.currentThread().getName() + "," + workerId + "号工人发生了意外...boss线程无法执行");
        }
      } catch (Exception e) {
        System.out.println(Thread.currentThread().getName() + "," + workerId + "异常处理好，boss线程可以执行。");
      }
      latch.countDown();
    }

  }

  /**
   * 不合格的工人，出了情况不向上级汇报。
   */
  class UnqualifiedWorker implements Runnable {

    CountDownLatch latch;
    private int    workerId;

    UnqualifiedWorker(CountDownLatch latch, int workerId) {
      this.latch = latch;
      this.workerId = workerId;
    }

    @Override
    public void run() {
      System.out.println(Thread.currentThread().getName() + "," + workerId + "号工人已经就绪");
      int waitTime = new Random().nextInt(9);
      try {
        Thread.sleep(waitTime);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      if (waitTime % 2 == 0 && waitTime >= 5) {
        System.out.println(Thread.currentThread().getName() + "," + workerId + "号工人发生了意外...boss线程无法执行");
        throw new RuntimeException(Thread.currentThread().getName() + "," + workerId + "号工人发生了意外...boss线程无法执行");
      }
      latch.countDown();
    }

  }

  /**
   * 老板
   */
  class Boss implements Runnable {
    CountDownLatch latch;

    Boss(CountDownLatch latch) {
      this.latch = latch;
    }

    @Override
    public void run() {
      try {
        latch.await();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(Thread.currentThread().getName() + "老板说：工人就位了，开始分配任务给工人...");
    }

  }

  /**
   * 经理人
   */
  class Manager implements Runnable {
    CountDownLatch latch;

    Manager(CountDownLatch latch) {
      this.latch = latch;
    }

    @Override
    public void run() {
      try {
        latch.await();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println(Thread.currentThread().getName() + "经理也说：工人就位了，开始分配任务给工人...");
    }

  }
}
