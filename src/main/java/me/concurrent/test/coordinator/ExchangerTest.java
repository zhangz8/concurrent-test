package me.concurrent.test.coordinator;

import java.util.concurrent.Exchanger;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、原理：<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月9日下午1:40:59
 */
public class ExchangerTest extends TestCase {

  @Test
  public void test1() throws InterruptedException {

    Exchanger<String> exchanger = new Exchanger<String>();

    Thread worker = new Thread(new Worker(exchanger), "Worker");
    Thread boss = new Thread(new Boss(exchanger), "Boss");
    boss.start();
    Thread.sleep(300);
    worker.start();

    Thread.sleep(3000);
  }

  class Worker implements Runnable {

    final Exchanger<String> exchanger;

    Worker(Exchanger<String> exchanger) {
      this.exchanger = exchanger;
    }

    @Override
    public void run() {
      for (int i = 10; i < 20; i++) {
        try {
          String cmd = exchanger.exchange("做出了水杯：" + i);
          System.out.println(Thread.currentThread().getName() + "接收到工作内容：" + cmd);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

  }

  class Boss implements Runnable {

    final Exchanger<String> exchanger;

    Boss(Exchanger<String> exchanger) {
      this.exchanger = exchanger;
    }

    @Override
    public void run() {
      for (int i = 0; i < 10; i++) {
        try {
          String result = exchanger.exchange("现在需要做出一个水杯：" + i);
          System.out.println(Thread.currentThread().getName() + "收到工人的工作成果：" + result);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

  }
}
