package me.concurrent.test.coordinator;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、原理：<br>
 * 构造时传入参与者数量和到达集合点要执行的线程，<br>
 * a.参与者线程启动后调用await()，获取锁权限，<br>
 * b.参与者数量减1，达到0时，执行到达集合点要执行的线程，同时标记（trip.signalAll()）上轮执行完毕， <br>
 * c.检测线程状态，等待（trip.await()）或中断或超时（后两个异常了会trip.signalAll()），最后释放锁。<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月8日下午5:14:56
 */
public class CyclicBarrierTest extends TestCase {

  public void test1() throws InterruptedException, BrokenBarrierException {
    int workerNum = 5;
    CyclicBarrier barrier = new CyclicBarrier(workerNum, new Boss());
    for (int i = 0; i < workerNum; i++) {
      Thread worder = new Thread(new Worker(barrier), "Worker" + i);
      worder.start();
    }
    Thread.sleep(2000);
    // barrier.reset();
    for (int i = 0; i < workerNum; i++) {
      Thread worder = new Thread(new Worker(barrier), "Worker" + i);
      worder.start();
    }

    Thread.sleep(2000);
  }

  class Boss implements Runnable {

    @Override
    public void run() {
      System.out.println("Boss:人齐了，正在分配工作给Worder。");
    }

  }

  class Worker implements Runnable {
    CyclicBarrier barrier;

    Worker(CyclicBarrier barrier) {
      this.barrier = barrier;
    }

    @Override
    public void run() {
      try {
        System.out.println(Thread.currentThread().getName() + ",准备好了。");
        barrier.await();
        System.out.println(Thread.currentThread().getName() + ",收到工作内容。");
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (BrokenBarrierException e) {
        e.printStackTrace();
      }
    }

  }

}
