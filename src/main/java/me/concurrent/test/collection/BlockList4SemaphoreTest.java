//package me.concurrent.test.collection;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.Semaphore;
//
//import org.junit.Test;
//
//import junit.framework.TestCase;
//
//public class BlockList4SemaphoreTest extends TestCase {
//
//  @Test
//  public void test1() throws InterruptedException {
//
//    Queue<Integer> queue = new Queue<Integer>(5);
//
//    for (int i = 0; i < 4; i++) {
//      Thread p = new Thread(new Provider(queue));
//      p.start();
//    }
//    for (int i = 0; i < 4; i++) {
//      Thread p = new Thread(new Consumer(queue));
//      p.start();
//    }
//
//    Thread.sleep(3000);
//  }
//
//  class Queue<E> {
//    final int capicity;
//    List<E>   list;
//
//    Semaphore mutex;
//    Semaphore putActionNum;
//    Semaphore getActionNum;
//
//    Queue(int capicity) {
//      this.capicity = capicity;
//      list = new ArrayList<E>(capicity);
//      mutex = new Semaphore(1);
//      putActionNum = new Semaphore(capicity);
//      getActionNum = new Semaphore(0);
//    }
//
//    public void put(E e) throws InterruptedException {
//      putActionNum.acquire();// put操作许可减1
//      mutex.acquire();
//      list.add(e);
//      mutex.release();
//      getActionNum.release();// get操作许可加1
//    }
//
//    public E take() throws InterruptedException {
//      E e = null;
//      getActionNum.acquire();// get操作许可减1
//      mutex.acquire();
//      if (list.size() > 0) {
//        e = list.get(0);
//        list.remove(0);
//      }
//      mutex.release();
//      putActionNum.release();// put操作许可加1
//      return e;
//    }
//  }
//
//  class Provider implements Runnable {
//    Queue<Integer> queue;
//
//    Provider(Queue<Integer> queue) {
//      this.queue = queue;
//    }
//
//    @Override
//    public void run() {
//      for (int i = 0; i < 10; i++) {
//        try {
//          queue.put(i);
//        } catch (InterruptedException e) {
//          // TODO Auto-generated catch block
//          e.printStackTrace();
//        }
//        System.out.println(Thread.currentThread().getName() + "put," + i);
//      }
//    }
//
//  }
//
//  class Consumer implements Runnable {
//    Queue<Integer> queue;
//
//    Consumer(Queue<Integer> queue) {
//      this.queue = queue;
//    }
//
//    @Override
//    public void run() {
//      for (int i = 0; i < 1000; i++) {
//        Integer v;
//        try {
//          v = queue.take();
//          System.out.println(Thread.currentThread().getName() + ",get," + v);
//        } catch (InterruptedException e) {
//          e.printStackTrace();
//        }
//      }
//    }
//
//  }
//
//}
