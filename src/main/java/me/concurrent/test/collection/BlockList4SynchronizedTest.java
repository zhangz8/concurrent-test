package me.concurrent.test.collection;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

/**
 * 测试目标：<br>
 * 1、使用synchronized实现阻塞队列。<br>
 * 2、wait、notify的使用。<br>
 * 在使用这个两个方法向，要先获取对象的锁，否则会抛IllegalMonitorStateException。
 */
public class BlockList4SynchronizedTest {

  @Test
  public void test1() throws InterruptedException {

    final Queue<Integer> queue = new Queue<Integer>(5);

    Thread p1 = new Thread(new Producer(queue), "Producer1");
    Thread p2 = new Thread(new Producer(queue), "Producer2");
    Thread p3 = new Thread(new Producer(queue), "Producer3");
    Thread p4 = new Thread(new Producer(queue), "Producer4");

    p1.setPriority(Thread.MAX_PRIORITY);
    p2.setPriority(Thread.MAX_PRIORITY);
    p3.setPriority(Thread.MAX_PRIORITY);
    p4.setPriority(Thread.MAX_PRIORITY);

    Thread c1 = new Thread(new Consumer(queue), "Consumer");
    c1.setPriority(Thread.MIN_PRIORITY);

    c1.start();
    p1.start();
    p2.start();
    p3.start();
    p4.start();

    Thread.sleep(3000);
  }

  class Queue<E> {
    final int     capicity;
    final List<E> list;

    Queue(int capicity) {
      this.capicity = capicity;
      this.list = new ArrayList<E>(capicity);
    }

    synchronized E take() throws InterruptedException {
      for (; list.size() == 0;) {
        wait();
      }

      E e = list.get(0);
      list.remove(0);
      notifyAll();
      return e;
    }

    synchronized void put(E e) throws InterruptedException {
      for (; list.size() >= capicity;) {
        wait();
      }

      list.add(e);
      notifyAll();
    }
  }

  class Producer implements Runnable {
    Queue<Integer> queue;

    Producer(Queue<Integer> queue) {
      this.queue = queue;
    }

    @Override
    public void run() {
      for (int i = 20; i < 30; i++) {
        try {
          System.out.println(Thread.currentThread().getName() + "," + i);
          queue.put(i);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

  }

  class Consumer implements Runnable {
    Queue<Integer> queue;

    Consumer(Queue<Integer> queue) {
      this.queue = queue;
    }

    @Override
    public void run() {
      for (int i = 0; i < 1000; i++) {
        try {
          int v = queue.take();
          System.out.println(Thread.currentThread().getName() + "," + v);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

  }
}
