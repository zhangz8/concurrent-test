package me.concurrent.test.collection;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、Condition原理： <br>
 * AQS两个队列：线程等待队列、条件等待队列。<br>
 * Condition只是一个条件，当调用await的时加入条件等待队列，释放AQS状态（state=0，释放锁），当前线程等待，给其它线程执行机会。
 * <br>
 * 调用singal方法，条件等待队列不为空时，把条件等待队列头节点指定下一个结点，并转移到AQS的线程等待队列，作为下一个被唤醒的节点。<br>
 * 2、Condition使用规则：<br>
 * 在调用Condition对象的方法时，首先要获取锁的所有权，否则会抛IllegalMonitorStateException<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月8日下午1:07:44
 */
public class BlockList4ReentrantLockTest extends TestCase {

  @Test
  public void test1() throws InterruptedException {
    final Queue<String> queue = new Queue<String>(10);
    Thread p1 = new Thread(new Producer(queue));
    Thread p2 = new Thread(new Producer(queue));
    Thread c1 = new Thread(new Consumer(queue));
    Thread c2 = new Thread(new Consumer(queue));
    Thread c3 = new Thread(new Consumer(queue));

    p1.start();
    p2.start();
    c1.start();
    c2.start();
    c3.start();
    Thread.sleep(30000);
  }

  class Queue<E> {

    final List<E> list;
    final int     capacity;

    ReentrantLock lock;
    Condition     notFull;
    Condition     notEmpty;

    public Queue(int capacity) {
      this.capacity = capacity;
      this.list = new ArrayList<E>(capacity);
      this.lock = new ReentrantLock();
      this.notEmpty = lock.newCondition();
      this.notFull = lock.newCondition();
    }

    void put(E e) throws InterruptedException {
      try {
        lock.lock();
        for (; list.size() >= capacity;) {
          notFull.await();
        }

        list.add(e);
        notEmpty.signal();
      } finally {
        lock.unlock();
      }
    }

    E take() throws InterruptedException {
      E e = null;
      try {
        lock.lock();
        for (; list.size() == 0;) {
          notEmpty.await();
        }
        e = list.get(0);
        list.remove(e);
        notFull.signal();
      } finally {
        lock.unlock();
      }
      return e;
    }

  }

  class Producer implements Runnable {
    Queue<String> queue;

    Producer(Queue<String> queue) {
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        for (int i = 0; i < 100; i++) {
          queue.put(i + "");
          System.out.println(Thread.currentThread().getName() + "write:" + i);
          Random r = new Random();
          int time = r.nextInt(9);
          Thread.sleep(time);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

  }

  class Consumer implements Runnable {
    Queue<String> queue;

    Consumer(Queue<String> queue) {
      this.queue = queue;
    }

    @Override
    public void run() {
      try {
        for (int i = 0; i < 1000; i++) {
          String data = queue.take();
          System.out.println(Thread.currentThread().getName() + "read:" + data);
          Random r = new Random();
          int time = r.nextInt(159);
          Thread.sleep(time);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

  }

}
