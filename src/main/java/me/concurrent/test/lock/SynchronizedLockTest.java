//package me.concurrent.test.lock;
//
//import org.junit.Test;
//
///**
// * 使用synchronized实现重入锁
// * 
// * @version 1.0.0
// * @author zhangz8@yeah.net
// * @date 2020年1月8日下午2:03:55
// */
//public class SynchronizedLockTest {
//
//  @Test
//  public void test1() throws InterruptedException {
//
//    SynchronizedLock lock = new SynchronizedLock();
//    Thread p = new Thread(new Provider(lock));
//    Thread c = new Thread(new Consumer(lock));
//    Thread c1 = new Thread(new Consumer(lock));
//
//    p.start();
//    c.start();
//    c1.start();
//
//    Thread.sleep(4000);
//  }
//
//}
//
//class SynchronizedLock {
//
//  Thread thread;
//
//  public synchronized void lock() throws InterruptedException {
//    Thread current = Thread.currentThread();
//    if (thread == null) {
//      thread = current;
//    }
//    if (thread != current) {
//      System.out.println(current.getName() + ",线程等待");
//      wait();
//    } else {
//      System.out.println(current.getName() + ",获取锁成功");
//    }
//  }
//
//  public synchronized void unlock() throws InterruptedException {
//    if (thread != null) {
//      Thread current = Thread.currentThread();
//      if (thread != current) {
//        wait();
//      } else {
//        System.out.println(current.getName() + ",获取锁成功");
//      }
//    }
//  }
//}
//
//class Provider implements Runnable {
//
//  SynchronizedLock lock;
//
//  Provider(SynchronizedLock lock) {
//    this.lock = lock;
//  }
//
//  @Override
//  public void run() {
//    try {
//      lock.lock();
//      System.out.println("Provider执行");
//      lock.unlock();
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
//  }
//
//}
//
//class Consumer implements Runnable {
//
//  SynchronizedLock lock;
//
//  Consumer(SynchronizedLock lock) {
//    this.lock = lock;
//  }
//
//  @Override
//  public void run() {
//    try {
//      lock.lock();
//      System.out.println("Consumer执行");
//      lock.unlock();
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
//  }
//
//}
