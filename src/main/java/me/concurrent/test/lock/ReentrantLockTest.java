package me.concurrent.test.lock;

import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、重入锁原理：<br>
 * 调用lock()时，尝试把AQS状态设置成1，如果成功说明锁获取到，否则线程进入等待队列。 <br>
 * 调用unlock时，把AQS状态调协成0，AQS持有者调置为null，唤醒等待队列的下一个末取消的节点（遇到取消的节点从队尾向队首查找）。<br>
 * 2、为什么说ReentrantLock是可重入的？<br>
 * 一个线程获取锁成功后，没有释放前，可以多次获取，AQS计数器递增。<br>
 * 3、与synchronized的异同：<br>
 * synchronized是悲观锁ReentrantLock是乐观锁。 <br>
 * ReentrantLock锁释放要放在finally块中，否则异常会死锁。<br>
 * 实现方式不同，synchronized是jvm级实现，ReentrantLock是api级实现。<br>
 * ReentrantLock可以提供公平锁支持，而synchronized不行。<br>
 * ReentrantLock可以提供读写锁，性能更优。<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月8日下午3:01:39
 */
public class ReentrantLockTest extends TestCase {

  @Test
  public void test1() throws InterruptedException {
    ReentrantLock lock = new ReentrantLock();
    Thread worker = new Thread(new Worker(lock));
    Thread boss = new Thread(new Boss(lock));
    worker.start();
    Thread.sleep(500);
    boss.start();

    Thread.sleep(3000);
  }

  class Boss implements Runnable {
    ReentrantLock lock;

    Boss(ReentrantLock lock) {
      this.lock = lock;
    }

    @Override
    public void run() {
      try {
        lock.lock();
        System.out.println("Boss线程进入");
        for (int i = 0; i < 10; i++) {
          System.out.println(i + "");
        }
      } finally {
        lock.unlock();
        System.out.println("Boss线程执行完毕");
      }
    }

  }

  class Worker implements Runnable {
    ReentrantLock lock;

    Worker(ReentrantLock lock) {
      this.lock = lock;
    }

    @Override
    public void run() {
      try {
        lock.lock();
        System.out.println("Worker线程进入");
        for (int i = 10; i < 50; i++) {
          System.out.println(i + "");
          Thread.sleep(50);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        lock.unlock();
        System.out.println("Worker线程执行完毕");
      }
    }

  }
}
