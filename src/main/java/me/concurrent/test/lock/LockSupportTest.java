//package me.concurrent.test.lock;
//
//import java.util.Random;
//import java.util.concurrent.locks.LockSupport;
//
//import junit.framework.TestCase;
//
//public class LockSupportTest extends TestCase {
//
//  public void test1() {
//
//  }
//
//  private void sleep(Thread t) {
//    Random r = new Random();
//    int time = r.nextInt(50);
//    try {
//      t.sleep(time);
//    } catch (InterruptedException e) {
//      e.printStackTrace();
//    }
//  }
//
//  class Provider implements Runnable {
//
//    @Override
//    public void run() {
//      System.out.println(Thread.currentThread().getName() + "线程进入");
//      sleep(Thread.currentThread());
//      System.out.println(Thread.currentThread().getName() + "线程执行完毕");
//    }
//
//  }
//
//  class Consumer implements Runnable {
//
//    @Override
//    public void run() {
//      System.out.println(Thread.currentThread().getName() + "线程进入");
//
//      System.out.println(Thread.currentThread().getName() + "线程执行完毕");
//    }
//
//  }
//
//  class Order {
//    Integer i = 0;
//
//    void order() {
//      int j = 0;
//      synchronized (i) {
//        j = i;
//      }
//      LockSupport.park();
//      sleep(Thread.currentThread());
//      LockSupport.unpark(Thread.currentThread());
//    }
//
//    void cancle() {
//      LockSupport.park();
//      sleep(Thread.currentThread());
//      LockSupport.unpark(Thread.currentThread());
//    }
//  }
//}
