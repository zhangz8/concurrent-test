package me.concurrent.test.threadlocal;

import org.junit.Test;

import io.netty.util.concurrent.FastThreadLocal;
import io.netty.util.concurrent.FastThreadLocalThread;
import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、FastThreadLocal优势在哪里？<br>
 * 使用常量下标记录FastThreadLocal中值，直接使用数据下标访问（时间复杂度O(1)），比ThreadLocal经过hash再查找数据要快一点点（时间复杂度O(2)），<br>
 * 发生虚拟机gcThreadLocal还需要清理弱引入（时间复杂度O(n)）。<br>
 * 另外，ThreadLocal发生hash冲突时，一步步试探（时间复杂度O(n)），直到数组位置为空，性能比FastThreadLocal直接使用下标存值差。<br>
 * 故：扩容时，都使用数组，速度相当，正常读写情况下，FastThreadLocal比ThreadLocal快出不少。<br>
 * 2、数据如何存储？<br>
 * 使用数组，构造时就确定了下标，直接存入。<br>
 * 3、如何扩容？<br>
 * FastThreadLocal的静态属性nextIndex为AtomicInteger，在构造时已经确定了存放下标，如果大于数组容量，开始扩容。<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月6日下午2:51:28
 */
public class FastThreadLocalTest extends TestCase {

  static FastThreadLocal<Integer> fastThreadLocal = new FastThreadLocal<Integer>() {
    protected Integer initialValue() throws Exception {
      return 1;
    };
  };

  @Test
  @SuppressWarnings("all")
  public void test1() throws InterruptedException {

    FastThreadLocalThread fastThread = new FastThreadLocalThread(new Runnable() {
      @Override
      public void run() {
        int v1 = fastThreadLocal.get();
        for (int i = 0; i < 32; i++) {
          if (i == 15) {// 触发扩容，注意观察前后值的变化
            fastThreadLocal.get();
          }
          fastThreadLocal = new FastThreadLocal<Integer>() {
            protected Integer initialValue() throws Exception {
              return 9;
            };
          };
          fastThreadLocal.get();
        }
        fastThreadLocal.get();
      }
    });

    fastThread.start();
  }

}
