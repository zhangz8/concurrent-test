package me.concurrent.test.threadlocal;

import org.junit.Test;

import junit.framework.TestCase;

/**
 * 测试目标：<br>
 * 1、ThreadLocal.ThreadLocalMap.Entity的rehash过程。达到容量的int 2/3。<br>
 * 2、hash冲突，如何解决的。查找Entity[]tables下一个不为空的位置存放。<br>
 * 3、Entity弱引用，gc过后是如何处理tables[]里的值。清理Entity[]tables里key为null的元素。<br>
 * 4、ThreadLocal的最佳实践。最好在使用后手动调用remove方法。<br>
 * 5、ThreadLocal原理（如何绑定到线程上去的）。<br>
 * 每次在使用里面的数据，都会先get再使用，get时是动态获取当前线程，Thread类中持有ThreadLoca.ThreadLocalMap对象引用，thread对象属性为则绑定<br>
 * 6、ThreadLocal瓶颈。netty有FastThreadLocal，使用确定的数组下标，代替hash表，性能快了一点点，但在高并发下，多次访问数据，性能会高出不少。<br>
 * 另外，没有使用弱引用，结构简单。<br>
 * 7、ThreadLocal不便之处。对象更新了，无法共享。<br>
 * <br>
 * 
 * 扩展：<br>
 * 1、hibernate使用ThreadLocal<Sesssion>管理session，解决性能问题同时保证线程安全。<br>
 * 2、SimpleDateFormat线程不安全，可以使用ThreadLocal<SimpleDateFormat><br>
 * 3、netty的FastThreadLocal<br>
 * 
 * 最佳实践：<br>
 * 1、不要ThreadLocal上绑定多个值，防止hash冲突带来的开销。<br>
 * 2、用完后手动remove，防止内存泄漏。<br>
 * 
 * @version 1.0.0
 * @author zhangz8@yeah.net
 * @date 2020年1月6日下午12:34:28
 */
public class ThreadLocalTest extends TestCase {
  static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {
                                            @Override
                                            protected Integer initialValue() {
                                              return 1;
                                            }
                                          };
  static ThreadLocal<String>  threadLoca2 = new ThreadLocal<String>() {
                                            @Override
                                            protected String initialValue() {
                                              return "abc";
                                            }
                                          };

  /**
   * 
   */
  @SuppressWarnings("all")
  @Test
  public void test1() {
    Thread t1 = new Thread(new Runnable() {
      @Override
      public void run() {
        Integer v0 = threadLocal.get();
        // 每new出来一个threadLocal对象，会放在entity数组中
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v1 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v3 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v4 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v5 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v6 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v7 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v8 = threadLocal.get();
        threadLocal = new ThreadLocal<Integer>() {
          @Override
          protected Integer initialValue() {
            return 2;
          }
        };
        Integer v9 = threadLocal.get();
        String v92 = threadLoca2.get();

        for (int i = 0; i < 10; i++) {
          threadLocal = new ThreadLocal<Integer>() {
            @Override
            protected Integer initialValue() {
              return 999;
            }
          };
          Integer v999 = threadLocal.get();// 第10次(i=0)get，会发生rehash
          if (i == 0) {
            System.gc();// gc过后，会清除entity的弱引用的key
          }
        }
      }
    });
    t1.start();// 使用新的线程，方便观察，因为main有一些附加的threadLocal变量会存在里面，影响观察

    Integer v2 = threadLocal.get();
  }
}
